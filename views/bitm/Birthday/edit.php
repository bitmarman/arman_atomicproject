<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;
$objBirthday = new \App\Birthday\Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view();


if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();



?>



<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Birth Information Update Form</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #f5f6f7;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}
        .bg-1 {
            background-color: #1abc9c; /* Green */
            color: #ffffff;
        }
        .bg-2 {
            background-color: #474e5d; /* Dark Blue */
            color: #ffffff;
        }
        .bg-3 {
            background-color: #ffffff; /* White */
            color: #555555;
        }
        .bg-4 {
            background-color: #2f2f2f; /* Black Gray */
            color: #fff;
        }
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: black;
        }
    </style>
</head>
<body><div class="container bg-1 text-center">
    <h1 style="color:#2f2f2f">Birth Information Update Form</h1>
    <h4 class="massage"><?php echo $msg;?></h4>
    <div class="form-group Form">
        <form action="update.php" method="post">
            <div>
                <h3>Enter Book Name:</h3>
                <input type="text" name="userName" value="<?php echo $oneData->user_name?>">
            </div>

            <div>
                <h3>Enter Author Name:</h3>
                <input type="date" name="birthDate" value="<?php echo $oneData->birth_date?>">
            </div>
            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            <input type="submit" class="btn btn-primary" value="Update">
        </form>
        </div>
    </div>

<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    $(document).ready(function () {
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
    })
</script>
</body>
</html>