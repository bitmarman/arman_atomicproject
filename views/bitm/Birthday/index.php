<?php
require_once("../../../vendor/autoload.php");

$objBirthday = new \App\Birthday\Birthday();
$allData = $objBirthday->index();


use App\Message\Message;


if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Birth Date - Active List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #3c763d;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}
        .bg-1 {
            background-color: #269abc; /* Blue */
            color: #333333;
        }
        .bg-2 {
            background-color: #475e4d; /* Dark Blue */
            color: #ffffff;
        }
        .bg-3 {
            background-color: #cccccc; /* White */
            color: #444444;
        }
        .bg-4 {
            background-color: #2f2f2f; /* red Gray */
            color: #2f2f2f;
        }
        .container {
            padding: 45px 5%;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: green;
        }
        td{
            border: 0px;
        }
        table{
            width: 90%;
        }
        .msg{
            height: 20px;
        }
        h1{
            font-weight: bold;
        }
        li{
            padding: 10px 5%;
        }
        .box{
            height: 35px;
        }
    </style>
</head>
<body class="bg-4">
<div class="container bg-1">
    <h1>Birth Date - Active List</h1>

    <ul class="nav nav-pills nav-justified">
        <li role="presentation" class="active"><a href="../atomicProject.html">Home</a></li>
        <li role="presentation" class="active"><a href="create.php">New Entry</a></li>
        <li role="presentation" class="active"><a href="trashed.php">Trashed List</a></li>
    </ul>

    <div class="text-center box"><h4 class="massage"><?php echo $msg;?></h4></div>
    <table class="table table-striped">

        <tr class="bg-3">
            <th>Serial Number</th>
            <th>ID</th>
            <th>Person Name</th>
            <th>Birth Date</th>
            <th>Action Buttons</th>
        </tr>
        <?php
        $serial=1;
        foreach ($allData as $oneData) {

            if($serial%2){
                $bgColor = "#1b6d85";
            }else{
                $bgColor = "#555555";
            }
            echo "
            <tr style='background-color: $bgColor' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->id</td>
                <td>$oneData->user_name</td>
                <td>$oneData->birth_date</td>
                <td><a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                <a href='edit.php?id=$oneData->id' class='btn btn-success'>Update</a>
                <a href='trash.php?id=$oneData->id' class='btn btn-primary'>Soft Delete</a></td>
            </tr>
        ";
            $serial++;
        }

        ?>
    </table>
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script>
        $(document).ready(function () {
            $('.massage').fadeOut(450);
            $('.massage').fadeIn(450);
            $('.massage').fadeOut(450);
            $('.massage').fadeIn(450);
            $('.massage').fadeOut(450);
            $('.massage').fadeIn(450);
            $('.massage').fadeOut(450);
        })
    </script>
</div>

</body>
</html>