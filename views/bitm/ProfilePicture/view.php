<?php


require_once("../../../vendor/autoload.php");
$objProfile = new \App\ProfilePicture\ProfilePicture();
$objProfile->setData($_GET);
$oneData = $objProfile->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #f5f6f7;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}
        .bg-1 {
            background-color: #1abc9c; /* Green */
            color: #ffffff;
        }
        .bg-2 {
            background-color: #474e5d; /* Dark Blue */
            color: #ffffff;
        }
        .bg-3 {
            background-color: #ffffff; /* White */
            color: #555555;
        }
        .bg-4 {
            background-color: #2f2f2f; /* Black Gray */
            color: #fff;
        }
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: #1abc9c;
        }
        .checkBox{
            margin: 0 40%;
        }
        img{
            max-width: 600px;
            max-height: 400px;
        }
    </style>
</head>
<body class="bg-4">
<div class="container bg-1">
<?php

echo "
<div class='container'>
<table width='500px' align='center'>
    <tr>
        <td>ID: </td>
        <td>$oneData->id</td>
    </tr>
    <tr>
        <td>Name: </td>
        <td>$oneData->user_name</td>
    </tr>
    <tr>
        <td>Profile Picture: </td>
        <td><img src='images/$oneData->profile_pic_link'></td>
    </tr>
</table>
</div>





";
?>
</div>
<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    $(document).ready(function () {
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
    })
</script>
    </body>
</html>