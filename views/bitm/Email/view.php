<?php
require_once("../../../vendor/autoload.php");

$objEmail = new \App\Email\Email();
$objEmail->setData($_GET);
$oneData = $objEmail->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Single Email Information</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #f5f6f7;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}
        .bg-1 {
            background-color: #1abc9c; /* Green */
            color: #ffffff;
        }
        .bg-2 {
            background-color: #474e5d; /* Dark Blue */
            color: #ffffff;
        }
        .bg-3 {
            background-color: #ffffff; /* White */
            color: #555555;
        }
        .bg-4 {
            background-color: #2f2f2f; /* Black Gray */
            color: #fff;
        }
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: black;
        }
        td{
            padding: 10px 20px;
        }
    </style>
</head>
<body class="bg-4">
<?php
echo "<div class='container'>
        <table class='bg-1'>
            <tr>
                <td>ID:</td>
                <td>$oneData->id</td>
        </tr>
        <tr>
            <td>Person Name: </td>
            <td> $oneData->user_name</td>
            </tr>
            <tr>
            <td>Email Address: </td>
            <td>$oneData->email_add</td>
        </tr>
        </table>
        </div>
    ";
?>
</body>
</html>
