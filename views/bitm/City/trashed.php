
<?php
require_once("../../../vendor/autoload.php");

$objCity = new \App\City\City();
$allData = $objCity->trashed();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>City Information - Trashed List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #f5f6f7;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}
        .bg-1 {
            background-color: #1abc9c; /* Green */
            color: #222222;
        }
        .bg-2 {
            background-color: #474e5d; /* Dark Blue */
            color: #ffffff;
        }
        .bg-3 {
            background-color: #ffffff; /* White */
            color: #1b6d85;
        }
        .bg-4 {
            background-color: #2f2f2f; /* Black Gray */
            color: #ffffff;
        }
        .container {
            padding: 45px 5%;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: black;
        }
        td{
            border: 0px;
        }
        table{
            width: 75%;
        }
        li{
            padding: 10px 5%;
        }
        h1{
            font-weight: bold;
        }
        .box{
            height: 35px;
        }
    </style>
</head>
<body class="bg-4">
<div class="container bg-1">
    <h1>City Information - Trashed List</h1>
    <ul class="nav nav-pills nav-justified">
        <li role="presentation" class="active"><a href="../atomicProject.html">Home</a></li>
        <li role="presentation" class="active"><a href="index.php">Active List</a></li>
        <li role="presentation" class="active"><a href="create.php">New Entry</a></li>
    </ul>
    <div class="box"></div>
    <table class="table table-striped">

        <tr>
            <th>Serial Number</th>
            <th>ID</th>
            <th>Division Name</th>
            <th>District Name</th>
            <th>City Name</th>
            <th>Action Butttons</th>
        </tr>
        <?php
        $serial=1;
        foreach ($allData as $oneData) {

            if($serial%2){
                $bgColor = "#1b6d85";
            }else{
                $bgColor = "#555555";
            }
            echo "
            <tr style='background-color: $bgColor' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->id</td>
                <td>$oneData->division</td>
                <td>$oneData->district</td>
                <td>$oneData->city</td>
                <td>
                <a href='recover.php?id=$oneData->id' class='btn btn-success'>Recover</a>
                <a href='delete.php?id=$oneData->id' class='btn btn-danger'>Delete</a>
                </td>
            </tr>
        ";
            $serial++;
        }

        ?>
    </table>
</div>

</body>
</html>