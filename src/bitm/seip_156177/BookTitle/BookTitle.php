<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 3:25 PM
 */

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;



class BookTitle extends DB
{
    private $id;
    private $book_name;
    private $author_name;

    public function setData($allPostData=null){

        if(array_key_exists("id", $allPostData)){
            $this->id= $allPostData['id'];

        }
        if(array_key_exists("bookName", $allPostData)){
            $this->book_name= $allPostData['bookName'];

        }
        if(array_key_exists("authorName", $allPostData)){
            $this->author_name= $allPostData['authorName'];

        }
    }


    public function store(){
        $arrayData = array($this->book_name, $this->author_name);
        $query = 'INSERT INTO book_title (book_name, author_name) VALUES (?,?)';


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been inserted successfully");
        }else{
            Message::setMessage("Failed! Data has not been inserted");
        }

        Utility::redirect('create.php');
    }



    public function index(){
        $sql = "Select * from book_title where soft_delete='No'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){
        $sql = "Select * from book_title where id=".$this->id;
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


        Utility::redirect('index.php');
    }


    public function trashed(){
        $sql = "Select * from book_title where soft_delete='yes'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }



    public function update(){
        $arrayData = array($this->book_name, $this->author_name);
        $query = 'UPDATE book_title SET book_name=?, author_name=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Updated successfully");
        }else{
            Message::setMessage("Failed! Data has not been Updated");
        }

        Utility::redirect('index.php');
    }




    public function trash(){
        $arrayData = array("Yes");
        $query = 'UPDATE book_title SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Trashed successfully");
        }else{
            Message::setMessage("Failed! Data has not been Trashed");
        }

        Utility::redirect('trashed.php');
    }


    public function recover(){
        $arrayData = array("No");
        $query = 'UPDATE book_title SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Recovered");
        }else{
            Message::setMessage("Failed! Data has not been Recovered");
        }

        Utility::redirect('index.php');
    }



    public function delete(){
        $sql = "DELETE from book_title where id=".$this->id;

        $result = $this->dbh->exec($sql);

        if($result){
            Message::setMessage("Success! Data has been Deleted");
        }else{
            Message::setMessage("Failed! Data has not been Deleted");
        }


        Utility::redirect('index.php');
    }

}