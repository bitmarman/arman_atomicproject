<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 2:00 AM
 */

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    private $id;
    private $user_name;
    private $filename;
    private $tempLocation;
    private $success;
    private $error;


    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("userName",$allPostData)){
            $this->user_name = $allPostData['userName'];
        }

    }
    public function setProPic($allFileData=null){
        if(array_key_exists("file",$allFileData)){


        $this->filename = $allFileData['file']['name'];
        $this->tempLocation = $allFileData['file']['tmp_name'];
        $src = "images/" .time().$this->filename;
        $this->success = move_uploaded_file($this->tempLocation, $src);
        $this->error = $allFileData['file']['error'];

    }
    }


    public function store(){
        $arrayData = array($this->user_name,time().$this->filename);
        $query = 'INSERT INTO profile_picture (user_name, profile_pic_link) VALUES (?,?)';

        $STH = $this->dbh->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('create.php');
    }
    public function index()
    {

        $sql = "SELECT * from profile_picture where soft_delete='No'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    

    public function view(){
        $sql = "Select * from profile_picture where id=".$this->id;
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


        Utility::redirect('index.php');
    }
    public function trashed(){
        $sql = "Select * from profile_picture where soft_delete='yes'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }



    public function update(){
        $arrayData = array($this->user_name,time().$this->filename);
        $sql = 'UPDATE profile_picture SET user_name=?, profile_pic_link=? WHERE id='.$this->id;
        $query = 'UPDATE profile_picture SET user_name=?, profile_pic_link=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Updated successfully");
        }else{
            Message::setMessage("Failed! Data has not been Updated");
        }

        Utility::redirect('index.php');
    }




    public function trash(){
        $arrayData = array("Yes");
        $query = 'UPDATE profile_picture SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Trashed successfully");
        }else{
            Message::setMessage("Failed! Data has not been Trashed");
        }

        Utility::redirect('index.php');
    }


    public function recover(){
        $arrayData = array("No");
        $query = 'UPDATE profile_picture SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Recovered");
        }else{
            Message::setMessage("Failed! Data has not been Recovered");
        }

        Utility::redirect('index.php');
    }



    public function delete(){
        $sql = "DELETE from profile_picture where id=".$this->id;

        $result = $this->dbh->exec($sql);

        if($result){
            Message::setMessage("Success! Data has been Deleted");
        }else{
            Message::setMessage("Failed! Data has not been Deleted");
        }


        Utility::redirect('trashed.php');
    }
}