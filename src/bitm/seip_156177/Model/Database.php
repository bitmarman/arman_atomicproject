<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 2:30 PM
 */

namespace App\Model;
use PDO, PDOException;


class Database
{

    public $dbh;


    public function __construct()
    {

        try{

            $this->dbh = new PDO('mysql:host=localhost;dbname=arman_atomicproject', "root", "");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        }

        catch(PDOException $error){


        }

    }

}