<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 3:25 PM
 */

namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class City extends DB
{
    private $id;
    private $division_name;
    private $district_name;
    private $city_name;

    public function setData($allPostData=null){

        if(array_key_exists("id", $allPostData)){
            $this->id= $allPostData['id'];

        }
        if(array_key_exists("division", $allPostData)){
            $this->division_name= $allPostData['division'];

        }
        if(array_key_exists("district", $allPostData)){
            $this->district_name= $allPostData['division'];

        }
        if(array_key_exists("cityName", $allPostData)){
            $this->city_name= $allPostData['cityName'];
        }
    }


    public function store(){
        $arrayData = array($this->division_name, $this->district_name, $this->city_name);
        $query = 'INSERT INTO city_info (division, district, city) VALUES (?, ?, ?)';


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been inserted successfully");
        }else{
            Message::setMessage("Failed! Data has not been inserted");
        }

        Utility::redirect('create.php');
    }

    public function index(){
        $sql = "Select * from city_info where soft_delete='No'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function view(){
        $sql = "Select * from city_info where id=".$this->id;
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


        Utility::redirect('index.php');
    }

    public function trashed(){
        $sql = "Select * from city_info where soft_delete='yes'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }



    public function update(){
        $arrayData = array($this->division_name, $this->district_name, $this->city_name);
        $query = 'UPDATE city_info SET division=?, district=?, city=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Updated successfully");
        }else{
            Message::setMessage("Failed! Data has not been Updated");
        }

        Utility::redirect('index.php');
    }




    public function trash(){
        $arrayData = array("Yes");
        $query = 'UPDATE city_info SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Trashed successfully");
        }else{
            Message::setMessage("Failed! Data has not been Trashed");
        }

        Utility::redirect('trashed.php');
    }


    public function recover(){
        $arrayData = array("No");
        $query = 'UPDATE city_info SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Recovered");
        }else{
            Message::setMessage("Failed! Data has not been Recovered");
        }

        Utility::redirect('index.php');
    }



    public function delete(){
        $sql = "DELETE from city_info where id=".$this->id;

        $result = $this->dbh->exec($sql);

        if($result){
            Message::setMessage("Success! Data has been Deleted");
        }else{
            Message::setMessage("Failed! Data has not been Deleted");
        }


        Utility::redirect('index.php');
    }


}